<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hari Pertama</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name:</label> <br> <br>
            <input type="text" name="Firstname"> <br><br>
        <label>Last Name:</label> <br><br>
            <input type="text" name="Lastname"><br><br>
        <label>Gender:</label><br><br>
            <input type="radio" name="Male">Male <br>
            <input type="radio" name="Female">Female <br>
            <input type="radio" name="Other">Other <br><br>
        <label for="">Nationality:</label><br><br>
            <select name="nationality" id="">
                <option value="1">Indonesian</option>
                <option value="2">Singaporean</option>
                <option value="3">Malaysian</option>
                <option value="4">Australian</option>
            </select><br><br>
        <label for="">Languange Spoken:</label><br><br>
            <input type="checkbox">Bahasa Indonesia <br>
            <input type="checkbox">English <br>
            <input type="checkbox">Other <br><br>
        <label for="">Bio:</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="kirim">
    </form>
</body>
</html>